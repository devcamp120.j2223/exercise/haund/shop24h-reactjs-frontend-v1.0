import {
  Grid,
  FormControlLabel,
  Typography,
  TextField,
  Button,
  Radio,
  RadioGroup,
  FormControl,
} from "@mui/material";
import { useEffect, useState } from "react";

import { useSelector, useDispatch } from "react-redux";
function ProductFilter() {
  const dispatch = useDispatch();
  const {
    taskFilterNameInput,
    taskFilterProductType,
    taskFilterMaxPriceInput,
    taskFilterMinPriceInput,
  } = useSelector((reduxData) => reduxData.filterReducer);
  const [productTypes, setProductTypes] = useState([]);
  const getData = async (paramUrl, paramOptions = {}) => {
    const response = await fetch(paramUrl, paramOptions);
    const responseData = await response.json();
    return responseData;
  };
  useEffect(() => {
    getData(
      `http://localhost:8000/productTypes`
    )
      .then((data) => {
        console.log(data.data);
        setProductTypes(data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  const inputProductChangeHandler = (event) => {
    dispatch({
      type: "TASK_INPUT_PRODUCT_CHANGE",
      value: event.target.value,
    });
  };
  const inputMaxPriceChangeHandler = (event) => {
    dispatch({
      type: "TASK_MAX_PRICE_PRODUCT_CHANGE",
      value: event.target.value,
    });
  };
  const inputMinPriceChangeHandler = (event) => {
    dispatch({
      type: "TASK_MIN_PRICE_PRODUCT_CHANGE",
      value: event.target.value,
    });
  };
  const onBtnFilterClick = () => {
    dispatch({
      type: "TASK_ON_FILTER_CLICK",
    });
  };
  const productTypeChangeHandler = (event) => {
    dispatch({
      type: "TASK_PRODUCT_TYPE_CHANGE",
      value: event.target.value,
    });
  };
  return (
    <Grid container p={2} >
      <Grid item xs={12} md={12} sm={12} lg={12}>
        <Typography gutterBottom variant="h6" component="div">
          Tên sản phẩm
        </Typography>
      </Grid>
      <Grid item xs={12} md={12} sm={12} lg={12}>
        <TextField
          size="small"
          fullWidth
          label="nhập tên sản phẩm"
          onChange={inputProductChangeHandler}
          value={taskFilterNameInput}
        />
      </Grid>
      <Grid item xs={12} md={12} sm={12} lg={12} mt={4}>
        <Typography gutterBottom variant="h6" component="div">
          Giá tiền
        </Typography>
      </Grid>

      <Grid item xs={12} md={12} sm={12} lg={12}>
        <TextField
          fullWidth
          size="small"
          label="Min"
          type="number"
          value={taskFilterMinPriceInput}
          onChange={inputMinPriceChangeHandler}
        />
      </Grid>

      <Grid item xs={12} md={12} sm={12} lg={12}>
        <Typography
          gutterBottom
          variant="h6"
          component="div"
          textAlign="center"
        >
          -
        </Typography>
      </Grid>
      <Grid item xs={12} md={12} sm={12} lg={12}>
        <TextField
          size="small"
          fullWidth
          label="Max"
          type="number"
          value={taskFilterMaxPriceInput}
          onChange={inputMaxPriceChangeHandler}
        />
      </Grid>
      <Grid item xs={12} md={12} sm={12} lg={12} mt={4}>
        <Typography gutterBottom variant="h6" component="div">
          Loại sản phẩm
        </Typography>
      </Grid>
      <Grid item xs={12} md={12} sm={12} lg={12}>
        <FormControl>
          <RadioGroup
            value={taskFilterProductType}
            name="radio-buttons-group"
            onChange={productTypeChangeHandler}
          >
            <FormControlLabel value="" control={<Radio />} label="None" />
            {
              productTypes.map((type, index) => (
                <FormControlLabel
                  value={type._id}
                  key={index}
                  control={<Radio />}
                  label={type.name}
                />
              ))
            }
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={12} sm={12} lg={12} mt={4}>
        <Button
          color="secondary"
          variant="contained"
          fullWidth
          onClick={onBtnFilterClick}
        >
          Tìm sản phẩm
        </Button>
      </Grid>
    </Grid>
  );
}
export default ProductFilter;
